/* eslint-disable */
import { FuseNavigationItem } from '@fuse/components/navigation';

export const defaultNavigation: FuseNavigationItem[] = [
    {
        id   : 'main',
        title: 'Main',
        type : 'basic',
        icon : 'heroicons_outline:user',
        link : '/main'
    }
];
export const compactNavigation: FuseNavigationItem[] = [
    {
        id   : 'main',
        title: 'Main',
        type : 'basic',
        icon : 'heroicons_outline:user',
        link : '/main'
    },
    {
        id   : 'chat',
        title: 'Chat',
        type : 'basic',
        icon : 'heroicons_outline:user',
        link : '/chat'
    }
];
export const futuristicNavigation: FuseNavigationItem[] = [
    {
        id   : 'main',
        title: 'Main',
        type : 'basic',
        icon : 'heroicons_outline:user',
        link : '/main'
    },
    {
        id   : 'chat',
        title: 'Chat',
        type : 'basic',
        icon : 'heroicons_outline:user',
        link : '/chat'
    }
];
export const horizontalNavigation: FuseNavigationItem[] = [
    {
        id   : 'main',
        title: 'Main',
        type : 'basic',
        icon : 'heroicons_outline:user',
        link : '/main'
    },
    {
        id   : 'chat',
        title: 'Chat',
        type : 'basic',
        icon : 'heroicons_outline:user',
        link : '/chat'
    }
];
