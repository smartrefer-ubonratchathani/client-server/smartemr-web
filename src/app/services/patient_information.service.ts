import { Injectable } from '@angular/core'
import axios from 'axios'
import { environment } from '../../environments/environment'


@Injectable({
  providedIn: 'root'
})
export class PatientInformationService {
    private axiosInstance = axios.create({
        baseURL: `${environment.apiUrl}/pt`
      })
    
      constructor () {
        this.axiosInstance.interceptors.request.use(config => {
          const token = sessionStorage.getItem('token')
          if (token) {
            config.headers['Authorization'] = `Bearer ${token}`
          }
          return config
        });
    
        this.axiosInstance.interceptors.response.use(response => {
          return response
        }, error => {
          return Promise.reject(error)
        })
      }
    
      async getList(query: any, limit: any, offset: any) {
        const url = `?query=${query}&limit=${limit}&offset=${offset}`
        return await this.axiosInstance.get(url)
      }

}