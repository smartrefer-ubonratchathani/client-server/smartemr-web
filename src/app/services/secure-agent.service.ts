import { Injectable } from '@angular/core'
import axios from 'axios'
import { environment } from '../../environments/environment'


@Injectable({
  providedIn: 'root'
})
export class SecureAgentService {
    private axiosInstance = axios.create({
        baseURL: `${environment.apiUrlSecureAgent}`
      })
    
      constructor () {
      }
    
      async readSamrtCard() {
        const url = `/api/smartcard/read`
        return await this.axiosInstance.get(url)
      }

}