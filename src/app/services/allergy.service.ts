import { Injectable } from '@angular/core'
import axios from 'axios'
import { environment } from '../../environments/environment'


@Injectable({
  providedIn: 'root'
})
export class AllergyService {
    private axiosInstance = axios.create({
        baseURL: `${environment.apiUrl}/pharmacy`
      })
    
      constructor () {
        this.axiosInstance.interceptors.request.use(config => {
          const token = sessionStorage.getItem('token')
          if (token) {
            config.headers['Authorization'] = `Bearer ${token}`
          }
          return config
        });
    
        this.axiosInstance.interceptors.response.use(response => {
          return response
        }, error => {
          return Promise.reject(error)
        })
      }
    
      async listAllergy(data: any) {
        const url = `/${data}`
        return await this.axiosInstance.get(url)
      }

}