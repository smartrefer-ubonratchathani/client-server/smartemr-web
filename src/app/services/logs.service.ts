import { Injectable } from '@angular/core'
import axios from 'axios'
import { environment } from '../../environments/environment'


@Injectable({
  providedIn: 'root'
})
export class LogsService {
    private axiosInstance = axios.create({
        baseURL: `${environment.apiUrl}/query`
      })
    
      constructor () {
        this.axiosInstance.interceptors.request.use(config => {
          const token = sessionStorage.getItem('token')
          if (token) {
            config.headers['Authorization'] = `Bearer ${token}`
          }
          return config
        });
    
        this.axiosInstance.interceptors.response.use(response => {
          return response
        }, error => {
          return Promise.reject(error)
        })
      }
      //แสดง Visit ที่มารับริการตามโรงพยาบาลที่เลือก
      async saveLogs(request_name: any,log_detail:any) {
        let data = {
          request_user:request_name,
          log_detail:log_detail
        }
        const url = `/saveLogs`
        return await this.axiosInstance.post(url,data)
      }

}