import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { Router } from '@angular/router';

@Component({
    selector   : 'app-root',
    templateUrl: './app.component.html',
    styleUrls  : ['./app.component.scss'],
    standalone : true,
    imports    : [RouterOutlet],
})
export class AppComponent
{
    /**
     * Constructor
     */
    constructor(private router:Router)
    {
        if(sessionStorage.getItem('token')){
            sessionStorage.setItem('accessToken',sessionStorage.getItem('token'));
        }else{
            this.router.navigate(['/sign-in']);
        }
    }
}
