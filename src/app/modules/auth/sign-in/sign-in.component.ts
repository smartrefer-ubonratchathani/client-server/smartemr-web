import { NgIf } from '@angular/common';
import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormsModule, NgForm, ReactiveFormsModule, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { fuseAnimations } from '@fuse/animations';
import { FuseAlertComponent, FuseAlertType } from '@fuse/components/alert';
import { AuthService } from 'app/core/auth/auth.service';
import { SweetAlertService } from '../../../services/sweetalert.service';
import { LoginService } from 'app/services/login.service';
import { UserService } from 'app/core/user/user.service';
import { LogsService } from 'app/services/logs.service';


@Component({
    selector     : 'auth-sign-in',
    templateUrl  : './sign-in.component.html',
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations,
    standalone   : true,
    imports      : [RouterLink, FuseAlertComponent, NgIf, FormsModule, ReactiveFormsModule, MatFormFieldModule, MatInputModule, MatButtonModule, MatIconModule, MatCheckboxModule, MatProgressSpinnerModule],
})
export class AuthSignInComponent implements OnInit
{
    @ViewChild('signInNgForm') signInNgForm: NgForm;

    alert: { type: FuseAlertType; message: string } = {
        type   : 'success',
        message: '',
    };
    signInForm: UntypedFormGroup;
    showAlert: boolean = false;

    /**
     * Constructor
     */
    constructor(
        private _activatedRoute: ActivatedRoute,
        private _authService: AuthService,
        private _formBuilder: UntypedFormBuilder,
        private _router: Router,
        private sweetAlertService: SweetAlertService,
        private _loginService: LoginService,
        private _userService: UserService,
        private _logsService: LogsService,
        
    )
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        // Create the form
        this.signInForm = this._formBuilder.group({
            username     : ['', Validators.required],
            password  : ['', Validators.required],
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Sign in
     */
    async signIn()
    {
        // Return if the form is invalid
        if ( this.signInForm.invalid )
        {
            return;
        }

        // Disable the form
        this.signInForm.disable();

        // Hide the alert
        this.showAlert = false;

        // Sign in
        let info: any = this.signInForm.value;
        // console.log(info);

        try {
            //ตัวแปร rs เพื่อเรียกใช้ class _loginService และฟังก์ชั่น login ส่งค่า token กลับมา
            let rs: any = await this._loginService.login(info);
            const data = rs.data;

            if(data.accessToken){
                sessionStorage.setItem('accessToken',data.accessToken);
                sessionStorage.setItem('fullname',data.user.name);
                this._userService.user = data.user;
                // การเก็บ logs access
/*
                try {
                    //ตัวแปร logs เพื่อเรียกใช้ class _logsService และฟังก์ชั่น saveLogs ส่งค่า token กลับมา
                    let logs: any = await this._logsService.saveLogs(data.user.name,'Access to Smart EMR');
                    const log_data = logs.data;
                    // console.log(log_data);
                } catch (error) {
                    console.log(error);
                    //messagebox fix
                    this.signInForm.enable();
                    this.sweetAlertService.error('คำชี้แจง', 'Log Error', 'Smart EMR');
                }
*/
                //console.log(sessionStorage.getItem('accessToken'));
                this.signInForm.enable();
                //OPEN Dashboard
                this._router.navigate(['/main']);

            } else {
                this.signInForm.enable();
                this.sweetAlertService.error('คำชี้แจง', 'Username/Password ไม่ถูกต้อง', 'Smart EMR');
            }

        } catch (error: any) {
            console.log(error);
            //messagebox fix
            this.signInForm.enable();
            this.sweetAlertService.error('คำชี้แจง', 'เชื่อมต่อ API ไม่ได้', 'Smart EMR');
        }

    }
}
