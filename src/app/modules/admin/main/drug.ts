{
    "ok": true,
    "rows": {
        "person": [
            {
                "cid": "1348700128410",
                "title": "ด.ญ.",
                "fname": "ไอรดา",
                "lname": "โพพิลา"
            }
        ],
        "allergy": [
            {
                "allergy_id": 40436,
                "drug_name": "//G-6-PD",
                "allergy_level_id": "",
                "report_date": "2019-08-23T17:00:00.000Z",
                "hospital_code": "10949",
                "is_active": true,
                "d_update": "2023-09-27T05:18:09.480Z",
                "tmt_id": "",
                "std_code": "",
                "symptom": "",
                "cid": "1348700128410",
                "hospital_name": "โรงพยาบาลน้ำยืน",
                "allergy_level_name": null
            },
            {
                "allergy_id": 74656,
                "drug_name": "G-6-PD",
                "allergy_level_id": null,
                "report_date": "2016-10-20T17:00:00.000Z",
                "hospital_code": "11443",
                "is_active": true,
                "d_update": "2023-09-28T09:01:18.454Z",
                "tmt_id": " ",
                "std_code": " ",
                "symptom": "เฝ้าระวังการใช้ยา",
                "cid": "1348700128410",
                "hospital_name": "โรงพยาบาลสมเด็จพระยุพราชเดชอุดม",
                "allergy_level_name": null
            }
        ],
        "drug": [],
        "g6pd": [
            {
                "g6pd_id": 2186,
                "cid": "1348700128410",
                "d_update": "2023-09-27T05:22:23.326Z",
                "is_active": true,
                "user_id": null,
                "note": null
            }
        ]
    }
}



{
    "ok": true,
    "rows": {
        "person": [
            {
                "cid": "3340500847985",
                "title": "",
                "fname": "พร",
                "lname": "บรรพตาที"
            }
        ],
        "allergy": [],
        "drug": [
            {
                "drugs_id": 332,
                "hospital_code": "10947",
                "drug_name": "STREPTOKINASE INJ. 1.5 M I.U.",
                "tmt_id": "107595",
                "std_code": "100552000005760110281273",
                "use_date": "2022-08-20T17:00:00.000Z",
                "is_active": true,
                "d_update": "2023-09-27T07:46:32.039Z",
                "cid": "3340500847985",
                "hospital_name": "โรงพยาบาลเขมราฐ"
            }
        ],
        "g6pd": []
    }
}